﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoVM.ViewModel
{
    /// <summary>
    /// 返回的功能类型
    /// </summary>
    public class MyReturn
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 返回的消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 返回的内容
        /// </summary>
        public object Data { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoVM.ViewModel
{
    /// <summary>
    /// 返回共用对象
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MyReturn1<T> where T : class
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 返回的消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 返回的内容
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// 帮助方法(成功)
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static MyReturn1<T> OK(T obj, string msg = "成功")
        {
            return new MyReturn1<T>() { Code = 200, Message = msg, Data = obj };
        }
        /// <summary>
        /// 帮助方法(失败)
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static MyReturn1<T> Error(T obj, string msg = "失败")
        {
            return new MyReturn1<T>() { Code = 500, Message = msg, Data = obj };
        }
    }
}

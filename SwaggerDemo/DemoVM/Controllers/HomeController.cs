﻿using DemoVM.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoVM.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {

        [HttpGet("user")]
        public MyReturn GetUser()
        {
            MyReturn myReturn = new MyReturn();
            User user = new User()
            {
                Id = 3,
                Name = "CC",
                Password = "123456",
            };

            myReturn.Data = user;
            myReturn.Code = 200;
            myReturn.Message = "成功";
            return myReturn;
        }

        /// <summary>
        /// 返回显示详细模型的对象
        /// </summary>
        /// <returns></returns>
        [HttpGet("user1")]
        public MyReturn1<User> GetUser1()
        {
            User user = new User()
            {
                Id = 3,
                Name = "CC",
                Password = "123456",
            };

            return MyReturn1<User>.OK(user);
        }
    }
}

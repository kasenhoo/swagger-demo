using demo2.Controllers;
using demo2.swagger;
using Demo2.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demo2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                options.Conventions.Add(new GroupNameActionModelConvention());
                options.Conventions.Add(new GroupNameControllerModelConvention());
            });

            services.AddCors();

            //新增
            services.AddSwaggerGen(c =>
            {
                //c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
                c.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Version = "v0.0.1",
                    Title = "项目v0.0.1",
                    Description = $"接口文档说明v0.0.1",
                    Contact = new OpenApiContact()
                    {
                        Name = "admin",
                        Email = "xxx@qq.com",
                        Url = null
                    }
                });

                c.SwaggerDoc("v2", new OpenApiInfo()
                {
                    Version = "v0.0.2",
                    Title = "项目v0.0.2",
                    Description = $"接口文档说明v0.0.2",
                    Contact = new OpenApiContact()
                    {
                        Name = "test",
                        Email = "xxxx@qq.com",
                        Url = null
                    }
                });
                c.IncludeXmlComments("Demo2.xml", true); //第二个参数true表示注释文件包含了控制器的注释
                //忽略掉声明为 过时的 方法
                c.SwaggerGeneratorOptions.IgnoreObsoleteActions = true;
                //同一组的排序方式
                c.OrderActionsBy(apiDescription => apiDescription.RelativePath.Length.ToString());
                //自定义SchemaId
                c.CustomSchemaIds(CustomSchemaIdSelector);
                /* 
                //标签页分组 使用请求类型分组
                c.TagActionsBy(apiDescription =>
                      new string[] { apiDescription.HttpMethod }
                );
                //修改Schema
                
                c.MapType<User>(() =>
                {
                    return new OpenApiSchema()
                    {
                        Type = "string"
                    };
                }); 
               

                c.AddServer(new OpenApiServer() { Url = "http://localhost:45811", Description = "地址1" });
                c.AddServer(new OpenApiServer() { Url = "http://127.0.0.1:45811", Description = "地址2" });
                //192.168.2.223是我本地IP
                c.AddServer(new OpenApiServer() { Url = "http://192.168.2.223:45811", Description = "地址3" });

                 */

                c.DocumentFilter<MyDocumentFilter>();


                //All和Get接口属于文档v1
                c.DocumentFilter<GroupNameDocumentFilter>(new object[] { "v1", new string[] { nameof(HomeController.Get) } });
                //All和Post接口属于v2
                c.DocumentFilter<GroupNameDocumentFilter>(new object[] { "v2", new string[] { nameof(HomeController.Post) } });
            });
        }

        /// <summary>
        /// 自定义自定义SchemaId
        /// </summary>
        /// <param name="modelType"></param>
        /// <returns></returns>
        private string CustomSchemaIdSelector(Type modelType)
        {
            //如果此对象不是构造泛型类型, 则返回 FullName
            if (!modelType.IsConstructedGenericType) return modelType.FullName.Replace("[]", "Array");

            //泛型类型的 则遍历
            var prefix = modelType.GetGenericArguments()
                .Select(genericArg => CustomSchemaIdSelector(genericArg))
                .Aggregate((previous, current) => previous + current);

            return prefix + modelType.FullName.Split('`').First();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            //开启跨域支持
            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            });


            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            //生成接口文件
            app.UseSwagger();
            //生成Web页面
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", "My API V1");
                c.SwaggerEndpoint("v2/swagger.json", "My API V2");

            });

            
        }
    }
}

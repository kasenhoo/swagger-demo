using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demo2.swagger
{
    public class MyDocumentFilter : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            //声明一个Scheme，注意下面的Id要和上面AddSecurityDefinition中的参数name一致
            var scheme = new OpenApiSecurityScheme()
            {
                Reference = new OpenApiReference() { Type = ReferenceType.SecurityScheme, Id = "JwtBearer" }
            };
            //注册全局认证（所有的接口都可以使用认证）
            swaggerDoc.SecurityRequirements.Add(new OpenApiSecurityRequirement()
            {
                [scheme] = new string[0]
            });



            swaggerDoc.Servers.Add(new OpenApiServer() { Url = "http://localhost:45811", Description = "地址1" });
            swaggerDoc.Servers.Add(new OpenApiServer() { Url = "http://127.0.0.1:45811", Description = "地址2" });
            //192.168.2.223是我本地IP
            swaggerDoc.Servers.Add(new OpenApiServer() { Url = "http://192.168.2.223:45811", Description = "地址3" });

        }
    }


}
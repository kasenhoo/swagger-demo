using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demo2.swagger
{
    public class GroupNameControllerModelConvention : IControllerModelConvention
    {
        public void Apply(ControllerModel controller)
        {
            if (controller.ControllerName == "Home")
            {
                foreach (var action in controller.Actions)
                {

                    if (action.ActionName == "Get")
                    {
                        action.ApiExplorer.GroupName = "v1";
                        action.ApiExplorer.IsVisible = true;
                    }
                    else if (action.ActionName == "Post")
                    {
                        action.ApiExplorer.GroupName = "v2";
                        action.ApiExplorer.IsVisible = true;
                    }
                }
            }
        }
    }
}
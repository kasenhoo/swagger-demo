using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demo2.swagger
{
    public class GroupNameDocumentFilter : IDocumentFilter
    {
        string documentName;
        string[] actions;

        public GroupNameDocumentFilter(string documentName, params string[] actions)
        {
            this.documentName = documentName;
            this.actions = actions;
        }


        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            foreach (var apiDescription in context.ApiDescriptions)
            {
                if (actions.Contains(apiDescription.ActionDescriptor.RouteValues["action"]))
                {
                    apiDescription.GroupName = documentName;
                }
            }
        }
    }


}
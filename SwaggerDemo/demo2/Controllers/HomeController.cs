using Demo2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demo2.Controllers
{
    /// <summary>
    ///测试控制器
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {

        /// <summary>
        /// 未使用ApiExplorerSettings特性，表名属于每一个swagger文档
        /// </summary>
        /// <returns>结果</returns>
        [HttpGet("All")]
        public string All()
        {
            return "All";
        }
        /// <summary>
        /// 使用ApiExplorerSettings特性表名该接口属于swagger文档v1
        /// </summary>
        /// <returns>Get结果</returns>
        [HttpGet]
        [ApiExplorerSettings(GroupName = "v1")]
        public string Get()
        {
            return "Get";
        }
        /// <summary>
        /// 使用ApiExplorerSettings特性表名该接口属于swagger文档v2
        /// </summary>
        /// <returns>Post结果</returns>
        [HttpPost]
        [ApiExplorerSettings(GroupName = "v2")]
        public string Post()
        {
            return "Post";
        }


        /// <summary>
        /// 使用ApiExplorerSettings特性表名该接口IgnoreApi 忽略不显示
        /// </summary>
        /// <returns>Get结果</returns>
        [HttpGet("IgnoreApi")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public string GetIgnoreApi()
        {
            return "Get";
        }



        /// <summary>
        /// 声明为过时的接口
        /// </summary>
        /// <returns></returns>
        [HttpGet("OldInfo")]
        [Obsolete]
        public string GetOldInfo()
        {
            return "Get";
        }


        /// <summary>
        /// 返回用户对象
        /// </summary>
        /// <returns></returns>
        [HttpGet("User")]
        public User GetUser()
        {
            return new User() { Id = 3333, Name = "admin", Password = "123456" };
        }
    }
}
